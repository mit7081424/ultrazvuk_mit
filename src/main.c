#include <main.h>
#include <utils.h>
#include <stm/stm8s.h>
#include <stdbool.h>
#include <stdio.h>

void rx_action(void) // will not compile without this event definition
{
    char c = UART1_ReceiveData8();
}

// External interrupt handler
INTERRUPT_HANDLER(EXTI_PORTD_IRQHandler, 6)
{
}

#define NOOP 0          // No operation
#define DIGIT0 1        // zápis hodnoty na 1. cifru
#define DIGIT1 2        // zápis hodnoty na 1. cifru
#define DIGIT2 3        // zápis hodnoty na 1. cifru
#define DIGIT3 4        // zápis hodnoty na 1. cifru
#define DIGIT4 5        // zápis hodnoty na 1. cifru
#define DIGIT5 6        // zápis hodnoty na 1. cifru
#define DIGIT6 7        // zápis hodnoty na 1. cifru
#define DIGIT7 8        // zápis hodnoty na 1. cifru
#define DECODE_MODE 9   // Aktivace/Deaktivace znakové sady (my volíme vždy hodnotu DECODE_ALL)
#define INTENSITY 10    // Nastavení jasu - argument je číslo 0 až 15 (větší číslo větší jas)
#define SCAN_LIMIT 11   // Volba počtu cifer (velikosti displeje) - argument je číslo 0 až 7 (my dáváme vždy 7)
#define SHUTDOWN 12     // Aktivace/Deaktivace displeje (ON / OFF)
#define DISPLAY_TEST 15 // Aktivace/Deaktivace "testu" (rozsvítí všechny segmenty)

// makra argumentů
// argumenty pro SHUTDOWN
#define DISPLAY_ON 1  // zapne displej
#define DISPLAY_OFF 0 // vypne displej
// argumenty pro DISPLAY_TEST
#define DISPLAY_TEST_ON 1  // zapne test displeje
#define DISPLAY_TEST_OFF 0 // vypne test displeje
// argumenty pro DECODE_MOD
#define DECODE_ALL 0b11111111 // (lepší zápis 0xff) zapíná znakovou sadu pro všechny cifry
#define DECODE_NONE 0         // vypíná znakovou sadu pro všechny cifry

#define TI1_PORT GPIOD
#define TI1_PIN GPIO_PIN_4

#define TRGG_PORT GPIOC
#define TRGG_PIN GPIO_PIN_1
#define TRGG_REVERSE GPIO_WriteReverse(TRGG_PORT, TRGG_PIN);

#define BUZZER_PORT GPIOD
#define BUZZER_PIN GPIO_PIN_6

#define CLK_GPIO GPIOD      // port na kterém je CLK vstup budiče
#define CLK_PIN GPIO_PIN_7  // pin na kterém je CLK vstup budiče
#define DATA_GPIO GPIOD     // port na kterém je DIN vstup budiče
#define DATA_PIN GPIO_PIN_3 // pin na kterém je DIN vstup budiče
#define CS_GPIO GPIOD       // port na kterém je LOAD/CS vstup budiče
#define CS_PIN GPIO_PIN_2   // pin na kterém je LOAD/CS vstup budiče

#define CLK_HIGH GPIO_WriteHigh(CLK_GPIO, CLK_PIN)
#define CLK_LOW GPIO_WriteLow(CLK_GPIO, CLK_PIN)
#define DATA_HIGH GPIO_WriteHigh(DATA_GPIO, DATA_PIN)
#define DATA_LOW GPIO_WriteLow(DATA_GPIO, DATA_PIN)
#define CS_HIGH GPIO_WriteHigh(CS_GPIO, CS_PIN)
#define CS_LOW GPIO_WriteLow(CS_GPIO, CS_PIN)

void max7219_send(uint8_t address, uint8_t data)
{
    uint8_t mask; // pomocná proměnná, která bude sloužit k procházení dat bit po bitu
    CS_LOW;       // nastavíme linku LOAD/CS do úrovně Low (abychom po zapsání všech 16ti bytů mohli vygenerovat na CS vzestupnou hranu)

    // nejprve odešleme prvních 8bitů zprávy (adresa/příkaz)
    mask = 0b10000000; // lepší zápis je: maska = 1<<7
    CLK_LOW;           // připravíme si na CLK vstup budiče úroveň Low
    while (mask)
    { // dokud jsme neposlali všech 8 bitů
        if (mask & address)
        {              // pokud má právě vysílaný bit hodnotu 1
            DATA_HIGH; // nastavíme budiči vstup DIN do úrovně High
        }
        else
        {             // jinak má právě vysílaný bit hodnotu 0 a...
            DATA_LOW; // ... nastavíme budiči vstup DIN do úrovně Low
        }
        CLK_HIGH;         // přejdeme na CLK z úrovně Low do úrovně High, a budič si zapíše hodnotu bitu, kterou jsme nastavili na DIN
        mask = mask >> 1; // rotujeme masku abychom v příštím kroku vysílali nižší bit
        CLK_LOW;          // vrátíme CLK zpět do Low abychom mohli celý proces vysílání bitu opakovat
    }

    // poté pošleme dolních 8 bitů zprávy (data/argument)
    mask = 0b10000000;
    while (mask)
    { // dokud jsme neposlali všech 8 bitů
        if (mask & data)
        {              // pokud má právě vysílaný bit hodnotu 1
            DATA_HIGH; // nastavíme budiči vstup DIN do úrovně High
        }
        else
        {             // jinak má právě vysílaný bit hodnotu 0 a...
            DATA_LOW; // ... nastavíme budiči vstup DIN do úrovně Low
        }
        CLK_HIGH;         // přejdeme na CLK z úrovně Low do úrovně High, a v budič si zapíše hodnotu bitu, kterou jsme nastavili na DIN
        mask = mask >> 1; // rotujeme masku abychom v příštím kroku vysílali nižší bit
        CLK_LOW;          // vrátíme CLK zpět do Low abychom mohli celý proces vysílání bitu opakovat
    }

    CS_HIGH; // nastavíme LOAD/CS z úrovně Low do úrovně High a vygenerujeme tím vzestupnou hranu (pokyn pro MAX7219 aby zpracoval náš příkaz)
}

void max7219_init(void)
{
    GPIO_Init(CS_GPIO, CS_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(CLK_GPIO, CLK_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(DATA_GPIO, DATA_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    // nastavíme základní parametry budiče
    max7219_send(DECODE_MODE, DECODE_ALL);        // zapnout znakovou sadu na všech cifrách
    max7219_send(SCAN_LIMIT, 3);                  // velikost displeje 8 cifer (počítáno od nuly, proto je argument číslo 7)
    max7219_send(INTENSITY, 5);                   // volíme ze začátku nízký jas (vysoký jas může mít velkou spotřebu - až 0.25A !)
    max7219_send(DISPLAY_TEST, DISPLAY_TEST_OFF); // Funkci "test" nechceme mít zapnutou
    max7219_send(SHUTDOWN, DISPLAY_ON);           // zapneme displej
}

void setup(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // taktovat MCU na 16MHz

    init_milis();
    init_uart1();

    /*----          trigger setup           ---------*/
    GPIO_Init(TRGG_PORT, TRGG_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(BUZZER_PORT, BUZZER_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);

    /*----           TIM2 setup           ---------*/
    TIM2_TimeBaseInit(TIM2_PRESCALER_16, 0xFFFF);
    /*TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);*/
    TIM2_Cmd(ENABLE);
    GPIO_Init(TI1_PORT, TI1_PIN, GPIO_MODE_IN_FL_NO_IT); // kanál 1 jako vstup
    TIM2_ICInit(TIM2_CHANNEL_1,                          // nastavuji CH1 (CaptureRegistr1)
                TIM2_ICPOLARITY_RISING,                  // náběžná hrana
                TIM2_ICSELECTION_DIRECTTI,               // CaptureRegistr1 bude ovládán z CH1
                TIM2_ICPSC_DIV1,                         // dělička je vypnutá
                0                                        // vstupní filter je vypnutý
    );
    TIM2_ICInit(TIM2_CHANNEL_2,              // nastavuji CH2 (CaptureRegistr2)
                TIM2_ICPOLARITY_FALLING,     // sestupná hrana
                TIM2_ICSELECTION_INDIRECTTI, // CaptureRegistr2 bude ovládán z CH1
                TIM2_ICPSC_DIV1,             // dělička je vypnutá
                0                            // vstupní filter je vypnutý
    );
    max7219_init();
}

int main(void)
{
    uint16_t time_start = 0;
    uint16_t time_end = 0;
    uint32_t diff;
    float dist = 0;

    setup();
    printf("Start\n");
    GPIO_WriteLow(BUZZER_PORT, BUZZER_PIN);

    max7219_send(DIGIT0, 0);
    max7219_send(DIGIT1, 0);
    max7219_send(DIGIT2, 0);
    max7219_send(DIGIT3, 0);
    max7219_send(DIGIT4, 0);
    max7219_send(DIGIT5, 0);
    max7219_send(DIGIT6, 0);
    max7219_send(DIGIT7, 0);

    uint32_t timestamp = 0;
    while (1)
    {
        if (milis() - timestamp > 100)
        {
            // trigger impulz pro spuštění měření
            GPIO_WriteHigh(TRGG_PORT, TRGG_PIN);
            delay_us(50);
            GPIO_WriteLow(TRGG_PORT, TRGG_PIN);

            while (TIM2_GetFlagStatus(TIM2_FLAG_CC1) == RESET)
                ;                            // čekám na náběžnou hranu
            TIM2_ClearFlag(TIM2_FLAG_CC1);   // smažu vlajku
            time_start = TIM2_GetCapture1(); // poznamenám si čas

            while (TIM2_GetFlagStatus(TIM2_FLAG_CC2) == RESET)
                ;                          // čekám na sestupnou hranu
            TIM2_ClearFlag(TIM2_FLAG_CC2); // smažu vlajku
            time_end = TIM2_GetCapture2(); // poznamenám si čas

            diff = (time_end - time_start); // délka impoulzu v mikrosekundách
            printf("%lu us\n", diff);
            dist = diff * 340 / 20000;
            diff = diff * 340 / 20000; // FixPoint přepočet na cm
            printf("%lu cm\n\n", diff);
            timestamp = milis();
        }
        if (diff < 5)
            GPIO_WriteHigh(BUZZER_PORT, BUZZER_PIN);
        else
            GPIO_WriteLow(BUZZER_PORT, BUZZER_PIN);
        uint8_t thou = (uint16_t)dist/1000;
        uint8_t hund = (uint16_t)(dist-(thou*1000))/100;
        uint8_t tens = (uint16_t)(dist-((thou*1000) + (hund*100)))/10;
        uint8_t units = (uint16_t)dist - (thou*1000) - (hund*100) - (tens * 10);
        max7219_send(DIGIT3, thou);
        max7219_send(DIGIT2, hund);
        max7219_send(DIGIT1, tens);
        max7219_send(DIGIT0, units);
        delay_ms(10);
    }
}

/*-------------------------------  Assert -----------------------------------*/
#include "stm/__assert__.h"
